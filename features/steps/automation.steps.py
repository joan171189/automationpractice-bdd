from behave import given, when, then
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import os
import time
from random import randint

@given ('user is on automationpractice website')
def step_start_page (context):
    context.driver.get('http://automationpractice.com/index.php')
    context.driver.implicitly_wait(30)
    context.driver.maximize_window()
@when('user types valid email and click create an account and fills in the Sign In form and submits it')
def step_sign_in(context):
    # 1
    context.driver.find_element_by_xpath('//*[@id="header"]/div[2]/div/div/nav/div[1]/a').click();
    # 2
    mail = str(randint(0,9999)) + '@19jo-mail.com'
    context.driver.find_element_by_xpath('//*[@id="email_create"]').send_keys(mail);
    # 3
    context.driver.find_element_by_xpath('//*[@id="SubmitCreate"]/span').click();
    # 4
    context.driver.find_element_by_xpath('//*[@id="id_gender2"]').click();
    # 5
    context.driver.find_element_by_xpath('//*[@id="customer_firstname"]').send_keys('Ola');
    # 6
    context.driver.find_element_by_xpath('//*[@id="customer_lastname"]').send_keys('Skoczeń');
    # 7
    context.driver.find_element_by_xpath('//*[@id="passwd"]').send_keys('1234567890');
    # 8
    select = Select(context.driver.find_element_by_xpath('//*[@id="days"]'))
    select.select_by_value('12');

    select = Select(context.driver.find_element_by_xpath('//*[@id="months"]'))
    select.select_by_value('4');

    select = Select(context.driver.find_element_by_xpath('//*[@id="years"]'))
    select.select_by_value('1965');
    # 9
    context.driver.find_element_by_xpath('//*[@id="address1"]').send_keys('Leśna');
    # 10
    context.driver.find_element_by_xpath('//*[@id="city"]').send_keys('Warsaw');
    # 11
    select = Select(context.driver.find_element_by_xpath('//*[@id="id_state"]'))
    select.select_by_visible_text('California');
    # 12
    context.driver.find_element_by_xpath('//*[@id="postcode"]').send_keys('32343');
    # 13
    select = Select(context.driver.find_element_by_xpath('//*[@id="id_country"]'))
    select.select_by_visible_text('United States');
    # 14
    context.driver.find_element_by_xpath('//*[@id="phone_mobile"]').send_keys('353634663');
    # 15
    context.driver.find_element_by_xpath('//*[@id="alias"]').clear()
    context.driver.find_element_by_xpath('//*[@id="alias"]').send_keys(mail);
    
    context.driver.find_element_by_tag_name('body')
    context.driver.save_screenshot("screenshots/TC 1 - 1.1.png")
    # 16
    context.driver.find_element_by_xpath('//*[@id="submitAccount"]/span').click();

@then('User can see his account')
def step_login(context):
    context.driver.save_screenshot("screenshots/TC 1 - 1.2.png")
    assert context.driver.find_element_by_tag_name('body')