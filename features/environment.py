from selenium import webdriver
 
 
def before_feature(context, feature):
    # webdriver for macOS, change if needed
    context.driver = webdriver.Chrome('features/steps/chromedriver')
    context.driver.implicitly_wait(3)
 
def afterfeature(context, feature):
    context.driver.quit()